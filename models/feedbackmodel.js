var mongoose = require("mongoose");
var feedbackSchema = mongoose.Schema;

var validateMessage = [function(val){
    return (val.length > 0);
}, 'Feedback message is not in correct format'];

var validateInputRequired = [function(val){
    return (val.length > 0);
}, 'Invalid json input'];

var feedbackModel = new feedbackSchema({
    UserQuery:{type: String, required:true, validate:validateInputRequired},
    Message: {type: Object, required:true, validate:validateMessage},
    Sentiment:{type: String, required:true, validate:validateInputRequired},            
    Conversation:{type: Object, required:true, validate:validateInputRequired},
    FeedbackType: {type: String},                                                       
    Intent: {type: String, required:true, validate:validateInputRequired},              
    IntentScore: {type: String},                                                        
    BotResponse: {type: String, required:true, validate:validateInputRequired},
    ApplicationId: {type: String, required:true, validate:validateInputRequired},
    ApplicationName: {type: String, required:true, validate:validateInputRequired},
    User: {type: String},
    Source: {type: String},
    
    CreatedBy: {type: String, default: "SysUser"},
    CreatedOn: {type: Date, default: Date.now} // Date time stamp when event was updated/ created
    
},{strict:false});

//export model instance 'Feedback', by providing model name 'feedbackModel' and collenctionname 'Feedback'
module.exports = mongoose.model('Feedback', feedbackModel,"Feedback");

