var	mongoose = require('mongoose'),
config = require('../config');

var db = mongoose.connect(config.mongo.uri);

process.on('SIGINT', function() {
	mongoose.connection.close(function () {
	  console.log('MongoDB connection closed.');
	  process.exit(0);
	});
  });