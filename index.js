var http = require('http'),
    url = require('url'),
    express = require('express'),
    bodyParser = require('body-parser'),
    config = require('./config'),
    twit = require('./lib/twitter'),
    connection = require('./models/connection'),
    feedbackModel = require('./models/feedbackmodel'),
    utility = require('./lib/utility');
    PythonShell = require('python-shell');


var app = express();
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.header('access-Control-Allow-Origin', '*');
    next();
});


app.get('/', function (req, res) {
    res.send('Welcome to FeedbackHandler api');
});


//*** Defining api router
var feedbackRouter = express.Router();
app.use('/api', feedbackRouter);

feedbackRouter.route('/sentiments/:feedback')
    .get(function (req, res) {
        var output = "";
        var options = {
            mode: 'text',
            //pythonPath: 'C:/Python34/python',
            scriptPath: './python/',
            args: [req.params.feedback]
        };
        PythonShell.run('NBSentimentClassifier.py', options, function (err, results) {
            if (err) throw err;
            output = results;
            res.send(output);
        });
    });

feedbackRouter.route('/tweetsummary')
    .get(function (req, res) {
        var tweets = twit.getTwits(res);

    })
feedbackRouter.route('/htmlsummary')
    .post(function (req, res) {
        var output = "";
        console.log(req.body);
        if (typeof req.body.url !== 'undefined') {
            var options = {
                mode: 'text',
                //pythonPath: 'C:/Python34/python',
                scriptPath: './python/',
                args: [req.body.url.value]
            };
            PythonShell.run('HTMLTextSummary.py', options, function (err, results) {
                if (err) throw err;
                output = results;
                res.send(output);
            });
        } else {
            res.send("Could not find url in request body");
        }

    });
feedbackRouter.route('/createsummarydocument')
    .post(function (req, res) {
        var output = "";
        if (typeof req.body.url !== 'undefined') {
            //console.log(req.body.url.value);
            var options = {
                mode: 'text',
                //pythonPath: 'C:/Users/manish-kumar5/AppData/Local/Programs/Python/Python36/python',
                scriptPath: './python/',
                args: [req.body.url.value]
            };
            PythonShell.run('create_pickle_url.py', options, function (err, results) {
                if (err) throw err;
                output = results;
                //console.log(output);
                res.send(output);
            });
        } else {
            res.send("Could not find url in request body");
        }
    });
feedbackRouter.route('/getsummaryfromdocument')
    .post(function (req, res) {
        var output = "";
        if (typeof req.body.question !== 'undefined') {
            //console.log(req.body.question.value);
            var options = {
                mode: 'text',
                //pythonPath: 'C:/Users/manish-kumar5/AppData/Local/Programs/Python/Python36/python',
                scriptPath: './python/',
                args: [req.body.question.value]
            };
            PythonShell.run('textextraction_extractive.py', options, function (err, results) {
                if (err) throw err;
                output = results;
                //console.log(output);
                res.send(output);
            });
        } else {
            res.send("Could not find url in request body");
        }
    });
feedbackRouter.route('/feedback')
    .get(function (req, res) {

        console.log("Feedback-Get request received..");
        var feedbackModel = require('./models/feedbackmodel');
        var query = feedbackModel.find(req.query);
        query.exec(function (err, results) {
            if (err) {
                console.log("Error during Feedback-Get request: " + err);
            }
            else {
                console.log("Feedback retrieved successfully.");
                res.status(200).json(results);
            }
        });
    })

    .post(function (req, res) {

        console.log("Feedback-Post request received..");
        var feedbackModel = require('./models/feedbackmodel');

        if ((utility.isJson(req.body)) && (typeof req.body.Feedback !== 'undefined')) {
            console.log('Feedback body identified');
            var feedback = req.body.Feedback;
            var feedback_type = feedback.FeedbackType;

            if (feedback_type == "feedback_positive")
                feedback.Sentiment = "Positive";
            else if (feedback_type == "feedback_negative")
                feedback.Sentiment = "Negative";
            else if (feedback_type == "feedback_submit") {
                console.log('This is feedback submit');
                var options = {
                    mode: 'text',
                    //pythonPath: 'C:/Python34/python',
                    scriptPath: './python/',
                    args: [feedback.Message]
                };
                PythonShell.run('NBSentimentClassifier.py', options, function (err, results) {
                    if (err) throw err;
                    feedback.Sentiment = results; //.replace(/\n|\r/g, "");
                    console.log('Feedback sentiment: ' + feedback.Sentiment);
                    var fb = new feedbackModel(feedback);
                    fb.save(function (err) {
                        if (err) {
                            console.log("Error during Feedback-Post request: " + err);
                            res.status(400).json({ success: "false", message: err });
                        }
                        else {
                            console.log("Feedback saved successfully.");
                            res.status(200).json({ success: "true", message: "Feedback saved successfully." });
                        }
                    });
                });
            }
            if (feedback_type != "feedback_submit") {
                var fb = new feedbackModel(feedback);
                fb.save(function (err) {
                    if (err) {
                        console.log("Error during Feedback-Post request: " + err);
                        res.status(400).json({ success: "false", message: err });
                    }
                    else {
                        console.log("Feedback saved successfully.");
                        res.status(200).json({ success: "true", message: "Feedback saved successfully." });
                    }
                });
            }

        }
        else {
            console.log("Input JSON is not of Feedback type.");
        }
    });


var port = process.env.PORT || config.port;
app.listen(port, function () {
    console.log('----------------------');
    console.log('Running on port : ' + config.port);
    console.log('----------------------');
});