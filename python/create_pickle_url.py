import pickle
from bs4 import BeautifulSoup
import requests
from pathlib import Path
import sys

def download_document(url):
    """Downloads document using BeautifulSoup, extracts the subject and all
    text stored in paragraph tags
    """
    r = requests.get(url)
    soup = BeautifulSoup(r.text, 'html.parser')
    title = soup.find('title').get_text()
    document = ' '.join([p.get_text() for p in soup.find_all('p')])
    return document

def load_pickledata(pkl_filename):
    _content = ""
    _filename = pkl_filename
    _file = Path(_filename)
    if _file.is_file():
        content_pkl_filename = _filename
        content_pkl = open(content_pkl_filename, 'rb')
        _content += pickle.load(content_pkl)
    return _content

def update_pickle(url, pkl_file):
    document = download_document(url)
    document += load_pickledata(pkl_file)
    urlcontent_pkl_filename = pkl_file
    urlcontent_pkl = open(urlcontent_pkl_filename, 'wb')
    pickle.dump(document, urlcontent_pkl)
    urlcontent_pkl.close()
    return "success"


#url = 'https://www.mercer.com/what-we-do/wealth-and-investments/employee-financial-wellness.html'
url = sys.argv[1]
pkl_file = "./python/urlcontent_v1.pkl"
_success = update_pickle(url, pkl_file)
print(_success)


