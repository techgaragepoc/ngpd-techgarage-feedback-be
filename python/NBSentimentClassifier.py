
# coding: utf-8

# In[28]:


import pickle
import sys


# In[29]:


nb_classifier_pkl_filename = "./python/nb_classifier_v1.pkl"
nb_classifier_pkl = open(nb_classifier_pkl_filename, 'rb')
trainedNBClassifier = pickle.load(nb_classifier_pkl)


# In[30]:


vocabulary_filename = "./python/vocabulary_v1.pkl"
vocabulary_pkl = open(vocabulary_filename, 'rb')
vocabulary = pickle.load(vocabulary_pkl)


# In[31]:


def extract_features(review):
    review_words = set(review)
    features = {}
    for word in vocabulary:
        features[word] = (word in review_words)
    return features


# In[32]:


def nbSentimentCalculator(review):
    problemInstance = review.split()
    problemFeatures = extract_features(problemInstance)
    return trainedNBClassifier.classify(problemFeatures)


# In[33]:

review = sys.argv[1]
#review = "Can we please discuss process questions as well? It may well come up."
print(nbSentimentCalculator(review))
