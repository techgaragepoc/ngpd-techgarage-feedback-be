
# coding: utf-8

# In[51]:


import nltk
import pickle


# In[52]:


file1 = "C:/Users/manish-kumar5/Downloads/sentiment labelled sentences/amazon_cells_labelled.txt"
file2 = "C:/Users/manish-kumar5/Downloads/sentiment labelled sentences/imdb_labelled.txt"
file3 = "C:/Users/manish-kumar5/Downloads/sentiment labelled sentences/yelp_labelled.txt"
file4 = "C:/Users/manish-kumar5/Downloads/sentiment labelled sentences/feedback_negative.txt"
file5 = "C:/Users/manish-kumar5/Downloads/sentiment labelled sentences/feedback_positive.txt"
file6 = "C:/Users/manish-kumar5/Downloads/sentiment labelled sentences/suggession.txt"


# In[53]:


with open(file1, 'r') as f:
    reviews_1 = f.readlines()
with open(file2, 'r') as f:
    reviews_2 = f.readlines()
with open(file3, 'r') as f:
    reviews_3 = f.readlines()
with open(file4, 'r') as f:
    reviews_4 = f.readlines()
with open(file5, 'r') as f:
    reviews_5 = f.readlines()
with open(file6, 'r') as f:
    reviews_6 = f.readlines()
len(reviews_5)


# In[54]:


total_reviews = reviews_1 + reviews_2 + reviews_3
negative_reviews = []
positive_reviews = []
neutral_reviews = []

#for review in total_reviews:
#    feedback = review.split('\t')
#    if feedback[1] == '0\n':
#        negative_reviews += [ feedback[0] ]
#    else:
#        positive_reviews.append(feedback[0])

negative_reviews += reviews_4
positive_reviews += reviews_5
neutral_reviews += reviews_6
cutoff = 2000

trainset_negative = negative_reviews[:cutoff]
trainset_positive = positive_reviews[:cutoff]
trainset_neutral = neutral_reviews[:cutoff]
testset_negative = negative_reviews[cutoff + 1:]
testset_positive = positive_reviews[cutoff + 1:]
testset_neutral = neutral_reviews[cutoff + 1:]
len(testset_negative)


# In[55]:


def getvocabulary():
    wordlist_positive = [word for line in trainset_positive for word in line.split()]
    wordlist_negative = [word for line in trainset_negative for word in line.split()]
    wordlist_neutral = [word for line in trainset_negative for word in line.split()]
    allwordlist = [item for sublist in [wordlist_positive, wordlist_negative, wordlist_neutral] for item in sublist]
    wordset = list(set(allwordlist))
    return wordset
vocabulary = getvocabulary()


# In[56]:


def getTrainingData():
    negTaggedTrainingReviewList = [{'review': oneReview.split(), 'label': 'negative'} for oneReview in trainset_negative]
    posTaggedTrainingReviewList = [{'review': oneReview.split(), 'label': 'positive'} for oneReview in trainset_positive]
    neuTaggedTrainingReviewList = [{'review': oneReview.split(), 'label': 'neutral'} for oneReview in trainset_neutral]
    fullTaggedTrainingData = [item for sublist in [negTaggedTrainingReviewList, posTaggedTrainingReviewList, neuTaggedTrainingReviewList] for item in sublist]
    trainingData = [(review['review'], review['label']) for review in fullTaggedTrainingData]
    return trainingData

trainingdata = getTrainingData()


# In[57]:


def extract_features(review):
    review_words = set(review)
    features = {}
    for word in vocabulary:
        features[word] = (word in review_words)
    return features


# In[58]:


def getTrainedNaiveBayesClassifier(extract_features, trainingdata):
    trainingFeatures = nltk.classify.apply_features(extract_features, trainingdata)
    trainedNBClassifier=nltk.NaiveBayesClassifier.train(trainingFeatures)
    return trainedNBClassifier
trainedNBClassifier = getTrainedNaiveBayesClassifier(extract_features, trainingdata)


# ## Dump the trained decision tree classifier with Pickle

# In[59]:


nbclassifier_pkl_filename = 'nb_classifier_v1.pkl'
nbclassifier_pkl = open(nbclassifier_pkl_filename, 'wb')
pickle.dump(trainedNBClassifier, nbclassifier_pkl)
nbclassifier_pkl.close()


# ## Dump vocabulary to pickle

# In[60]:


vocabulary_pkl_filename = 'vocabulary_v1.pkl'
vocabulary_pkl = open(vocabulary_pkl_filename, 'wb')
pickle.dump(vocabulary, vocabulary_pkl)
vocabulary_pkl.close()

