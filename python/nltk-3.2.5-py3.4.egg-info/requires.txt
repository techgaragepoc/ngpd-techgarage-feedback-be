six

[twitter]
twython

[all]
matplotlib
scikit-learn
pyparsing
scipy
python-crfsuite
twython
requests
numpy
gensim

[corenlp]
requests

[plot]
matplotlib

[machine_learning]
gensim
numpy
python-crfsuite
scikit-learn
scipy

[tgrep]
pyparsing