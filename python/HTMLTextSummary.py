
# coding: utf-8

# In[236]:


import nltk
from nltk.tokenize import word_tokenize, sent_tokenize
from nltk.corpus import stopwords
from string import punctuation
from nltk.collocations import *
from urllib.request import urlopen
from bs4 import BeautifulSoup
from nltk.probability import FreqDist
from heapq import nlargest
from collections import defaultdict
import unicodedata
import sys


# In[237]:


_stopwords = set(stopwords.words('english') + list(punctuation))


# In[238]:


def remove_nonlatin(s): 
    s = (ch for ch in s
         if unicodedata.name(ch).startswith(('LATIN', 'DIGIT', 'SPACE')))
    return ''.join(s)
def getTextFromHTML(articleurl):
    page = urlopen(articleurl).read().decode('utf-8', 'ignore')
    soup = BeautifulSoup(page, 'html5lib')
    text = ' '.join(map(lambda p: p.text, soup.find_all('p')))

    text =  text.replace('\n', ' ').replace('\xa0a','').replace('\t', '').replace('\xa0','')
    sents = sent_tokenize(text)
    for i,sent in enumerate(sents):
        sents[i] = remove_nonlatin(sent)
        sents[i] = " ".join(sents[i].split())
    word_sent = word_tokenize(text.lower())
    word_sent = [word for word in word_sent if word not in _stopwords]
    freq = FreqDist(word_sent)
    ranking = defaultdict(int)
    for i,sent in enumerate(sents):
        for w in word_tokenize(sent.lower()):
            if w in freq:
                ranking[i] += freq[w]
    sents_idx = nlargest(5, ranking, key=ranking.get)
    return [sents[j] for j in sorted(sents_idx)]


# In[239]:


#articleurl = "http://www.zdnet.com/article/3d-vision-and-ai-are-about-to-solve-the-biggest-problem-in-construction/"
articleurl = sys.argv[1]
print(getTextFromHTML(articleurl))

