
# coding: utf-8

# In[40]:


import nltk
from nltk.tokenize import word_tokenize, sent_tokenize
#from string import punctuation
from nltk.collocations import *
from nltk.probability import FreqDist
from heapq import nlargest
from collections import defaultdict
from difflib import SequenceMatcher
import sys


# In[41]:
with open('./python/stopword_english.txt', 'r') as f:
        text = f.readlines()
_stopwords = set(text) #set(text + list(punctuation))


# In[42]:


def getTextFromFile(path):
    file = path
    str1=""
    with open(file, 'r') as f:
        file = path
        with open(file, 'r') as f:
            text = f.readlines()
            text = set(text)
            for item in text:
                str1 +=  " " + str(item)
    return str1


# In[43]:


def getTextSummary(n, msg):
    list1 = []
    for sent in sent_tokenize(msg):
        if sent not in list1:
            for item in sent.split('\n'):
                if item not in list1:
                    list1.append(item)
    
    word_sents = word_tokenize(msg)
    word_sents = (word for word in word_sents if word not in _stopwords)
    freq = FreqDist(word_sents)
    ranking = defaultdict(int)
    for i,sent in enumerate(list1):
        for w in word_tokenize(sent.lower()):
            ranking[i] += freq[w]
    sents_idx = nlargest(n, ranking, key = ranking.get)
    summary_list = [list1[j] for j in sorted(sents_idx)]
    output_summary = []
    for summaryitem in summary_list:
        similar = "false"
        for item in output_summary:
            similarity = SequenceMatcher(None, summaryitem, item).ratio()
            if similarity > 0.6:
                similar = "true"
        if similar == "false":
            output_summary.append(summaryitem)
    return output_summary

# In[44]:


#message = getTextFromFile("C:/MercerOS/ChatBot/HRBOT/ngpd-techgarage-feedback-be/test.txt")
message = sys.argv[1]
print(getTextSummary(10, message))

