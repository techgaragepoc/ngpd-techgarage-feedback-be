from nltk import word_tokenize, pos_tag
from nltk.corpus import wordnet as wn
import heapq
from collections import Counter

def penn_to_wn(tag):
    """ Convert between a Penn Treebank tag to a simplified Wordnet tag """
    if tag.startswith('N'):
        return 'n'
 
    if tag.startswith('V'):
        return 'v'
 
    if tag.startswith('J'):
        return 'a'
 
    if tag.startswith('R'):
        return 'r'
 
    return None
 
def tagged_to_synset(word, tag):
    wn_tag = penn_to_wn(tag)
    if wn_tag is None:
        return None
 
    try:
        return wn.synsets(word, wn_tag)[0]
    except:
        return None
 
def sentence_similarity(sentence1, sentence2):
    """ compute the sentence similarity using Wordnet """
    # Tokenize and tag
    sentence1 = pos_tag(word_tokenize(sentence1))
    sentence2 = pos_tag(word_tokenize(sentence2))
 
    # Get the synsets for the tagged words
    synsets1 = [tagged_to_synset(*tagged_word) for tagged_word in sentence1]
    synsets2 = [tagged_to_synset(*tagged_word) for tagged_word in sentence2]
 
    # Filter out the Nones
    synsets1 = [ss for ss in synsets1 if ss]
    synsets2 = [ss for ss in synsets2 if ss]
    score, count = 0.0, 0
    best_score = [0.0]
    for ss1 in synsets1:
        for ss2 in synsets2:
            best1_score=ss1.path_similarity(ss2)
            if best1_score is not None:
                best_score.append(best1_score)
        max1=max(best_score)
        if best_score is not None:
            score += max1
            count += 1
        best_score=[0.0]
    # Average the values
    if count < 1:
        score = 0
    else:
        score /= count
    return score

 
sentences = [
    "Dogs are awesome.",
    "Some gorgeous creatures are felines.",
    "Dolphins are swimming mammals.",
    "Cats are beautiful animals.",
]
 
def symmetric_sentence_similarity(sentence1, sentence2):
    """ compute the symmetric sentence similarity using Wordnet """
    return (float(sentence_similarity(sentence1, sentence2) + sentence_similarity(sentence2, sentence1))) / 2 
def get_similar_sentences(focus_sentence, sentences):
    output_data = {}
    for sentence in sentences:
        output_data.update({sentence: symmetric_sentence_similarity(focus_sentence, sentence)})
    #print(output_data)
    return sorted(output_data, key=output_data.get, reverse=True)[:1]

#output = get_similar_sentences(focus_sentence, sentences)
#print(output)