from bs4 import BeautifulSoup
import requests
import re
import pickle
import nltk
from nltk.corpus import stopwords
stop = stopwords.words('english')
import short_sentence_similarity as ss
import sys
from pathlib import Path

# Noun Part of Speech Tags used by NLTK
# More can be found here
# http://www.winwaed.com/blog/2011/11/08/part-of-speech-tags/
NOUNS = ['NN', 'NNS', 'NNP', 'NNPS']
VERBS = ['VB', 'VBG', 'VBD', 'VBN', 'VBP', 'VBZ']

def download_document(url):
    """Downloads document using BeautifulSoup, extracts the subject and all
    text stored in paragraph tags
    """
    r = requests.get(url)
    soup = BeautifulSoup(r.text, 'html.parser')
    title = soup.find('title').get_text()
    document = ' '.join([p.get_text() for p in soup.find_all('p')])
    return document

def clean_document(document):
    """Remove enronious characters. Extra whitespace and stop words"""
    document = re.sub('[^A-Za-z .-]+', ' ', document)
    document = ' '.join(document.split())
    document = ' '.join([i for i in document.split() if i not in stop])
    return document

def tokenize_sentences(document):
    sentences = nltk.sent_tokenize(document)
    sentences = [nltk.word_tokenize(sent) for sent in sentences]
    return sentences

def word_freq_dist(document):
    """Returns a word count frequency distribution"""
    words = nltk.tokenize.word_tokenize(document)
    words = [word.lower() for word in words if word not in stop]
    fdist = nltk.FreqDist(words)
    return fdist

def extract_subject_question(question):
    # Get most frequent Nouns
    fdist = word_freq_dist(question)
    most_freq_nouns = [w for w, c in fdist.most_common(10)
                       if nltk.pos_tag([w])[0][1] in NOUNS]
    return most_freq_nouns


def tag_sentences(subject, document):
    """Returns tagged sentences using POS tagging"""
    # Tokenize Sentences and words
    sentences = tokenize_sentences(document)
    # Filter out sentences where subject is not present
    sentences_out = []
    for item in subject:
        sentences = [sentence for sentence in sentences if item in
                [word.lower() for word in sentence]]
        for text in sentences:
            sent = ' '.join(text)
            sent = sent.replace(" ’ s", "’s").replace(" ,",",").replace(" .", ".").replace(" ’", "’")
            sentences_out.append(sent)
    return set(sentences_out)


def get_matching_sentences(focus_sentence, document):
    sentences = nltk.sent_tokenize(document)
    output = []
    for sentence in sentences:
        score = ss.symmetric_sentence_similarity(focus_sentence, sentence)
        if(score > 0.5):
            output.append(sentence)
    return output

def find_match(question, document):
    #document = clean_document(document)
    subject = extract_subject_question(question)
    tagged_sents = tag_sentences(subject, document)
    matched_sents = get_matching_sentences(question, document)
    output_tagged = ss.get_similar_sentences(question, tagged_sents)
    output_matched = ss.get_similar_sentences(question, matched_sents)
    final_output = output_matched + list(set(output_tagged) - set(output_matched))
    if(len(final_output) < 1):
        return "No match found"
    else:
        return " ".join(final_output)
def load_pickledata(pkl_filename):
    _content = ""
    _filename = pkl_filename
    _file = Path(_filename)
    if _file.is_file():
        content_pkl_filename = _filename
        content_pkl = open(content_pkl_filename, 'rb')
        _content += pickle.load(content_pkl)
    return _content


pkl_filename = "./python/urlcontent_v1.pkl"
document = load_pickledata(pkl_filename)
#question = "how to improve health of organization and employees"
#question = "how to reduce absenteeism"
#question = "how to take care of financial wellness of employees"
#question = "Manish"
question = sys.argv[1]
answer = find_match(question, document)
print(answer)







