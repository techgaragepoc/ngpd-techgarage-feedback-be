var config = require('../config'),
    fs = require('fs'),
    Twit = require('twit'),
    PythonShell = require('python-shell');

var T = new Twit({
    consumer_key: config.twitter_consumer_key,
    consumer_secret: config.twitter_consumer_secret,
    access_token: config.twitter_access_token_key,
    access_token_secret: config.twitter_access_token_secret,
    timeout_ms: 60 * 1000,  // optional HTTP request timeout to apply to all requests.
})

module.exports = {
    getTwits: function (res) {
        var txt = "";
        T.get('search/tweets', { q: '@mercer', count: 100 }, function (err, data, response) {
            if (err) throw err;
            for (var item in data.statuses) {
                if (data.statuses[item].text && data.statuses[item].text != ""){
                    txt += data.statuses[item].text.replace(/(@\S+)/ig,"").replace(/(http\S+)/ig,"").replace(/[`~!@#$%^&*()_|+\-=÷¿?;:'",.<>\{\}\[\]\\\/]/gi, '')  + '\n';
                }
            }
            /*fs.writeFile('test.txt', txt, function (err) {
                if (err)
                    return console.log(err);
            });*/
            var options = {
                mode: 'text',
                //pythonPath: 'C:/Python34/python',
                scriptPath: './python/',
                args: txt
            };
            PythonShell.run('TextSummary.py', options, function (err, results) {
                if (err) res.send(err);
                res.send(results);
            });
        });
    }
}