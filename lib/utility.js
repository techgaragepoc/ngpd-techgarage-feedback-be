
var moment = require('moment'),
	config = require('../config');

module.exports = {
    isJson :    function (item) {
        item = typeof item !== "string"
            ? /* istanbul ignore next */JSON.stringify(item)
            : /* istanbul ignore next */item;

        try {
            item = JSON.parse(item);
        } catch (e) {
            return false;
        }
        /* istanbul ignore else */
        if (typeof item === "object" && item !== null) {
            return true;
        }

        return false;
    },
    isDate: function(item){
        return moment(item, config.logger.dateformat, true).isValid();
    },
    isDateTime: function(item){
        var dateTimeArray = item.split('T');
        if(dateTimeArray.length !== 2){
            return false;
        }else{
            var timeArray = dateTimeArray[1].split(' ');
            if(timeArray.length !== 2){
                return false;
            }
            if(isNaN(timeArray[1])){
                return false;
            }
            
            return moment(dateTimeArray[0] + " " + timeArray[0], config.logger.datetimeformat, true).isValid();

            
        }
    },
    getDateTime: function(item){
        var dateTimeArray = item.split('T');
        var timeArray = dateTimeArray[1].split(' ');
        return moment(dateTimeArray[0] + " " + timeArray[0],config.logger.datetimeformat).valueOf() + (timeArray[1] * 60 * 1000);
    },
    getDate: function(item){
        return moment(item, "MM-DD-YYYY").valueOf();
    },
    getDateString: function(item){
        return moment(item, "MM-DD-YYYY");
    },
    isMatchingJsonStr: function (obj1, obj2) {
        var map1 = {}, map2 = {};
        // get all properties in obj1 into a map
        getProps(obj1, map1);
        getProps(obj2, map2);
       
        for (var prop in map1) {
            if (prop in map2) {
               //
            }else{
                return false;
            }

        }
        return true;
    }
  
};

function isArray(item) {
            return Object.prototype.toString.call(item) === "[object Array]";
        }

function getProps(item, map) {
    if (typeof item === "object") {
        if (isArray(item)) {
            // iterate through all array elements
            for (var i = 0; i < item.length; i++) {
                getProps(item[i], map);
            }
        } else {
            for (var prop in item) {
                map[prop] = true;
                // recursively get any nested props
                // if this turns out to be an object or array
                getProps(item[prop], map);
            }
        }
    }
}